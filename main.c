/*  Projekt c.1
 *
 *  Matej Caja
 *  PPLS, 7.skupina
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAX 32
#define SUBOR "UCET.TXT"

//  uplny funkcny prototyp vsetkych funkcii pouzitych v projekte
FILE *otvor(FILE **fr);
FILE *zatvor(FILE **fr);
int zisti_pocet(FILE *fr);
int zisti_porovnaj(const void *prvy, const void *druhy);
void case_V(FILE *fr);
void case_R(FILE *fr);
double *alokuj_case_N(FILE *fr, int counter);
void case_S(FILE *fr, int pocitadlo, double *p_pole);
void case_H(double *p_pole, int pocitadlo);
void case_U(double *p_pole, int pocitadlo);

//  Main cita znaky z klavesnice a pomocou switch-u spusta funkciu ku danemu nacitanemu znaku, z cyklu sa vyskakuje pomocou
//  navestia goto;
int main()
{
    int c, counter = 0;
    FILE *fr = NULL, *subor = NULL;
    double *dynam_pole = NULL;

    while(1){
        c = getchar();
        switch(c){
            case 'V' :  fr = otvor(&subor);
                        case_V(fr);
                        counter = zisti_pocet(fr);
            break;

            case 'R' :  case_R(fr);
            break;

            case 'N' :  dynam_pole = NULL;
                        dynam_pole = alokuj_case_N(fr, counter);
            break;

            case 'S' :  case_S(fr, counter, dynam_pole);
            break;

            case 'H' :  case_H(dynam_pole, counter);
            break;

            case 'U' :  case_U(dynam_pole, counter);
            break;

            case 'K' :  fr = zatvor(&subor);
                        goto koniec;
            break;
        }
    }

    koniec:


    return 0;
}

//  funkcia na otvorenie suboru
FILE *otvor(FILE **fr)
{
    if((*fr = fopen(SUBOR, "r")) == NULL){
        printf("Neotvoreny subor\n");
        return NULL;
        }


    return *fr;
}

//  funkcia na zatvorenie suboru
FILE *zatvor(FILE **fr)
{
    if(fclose(*fr) == EOF){
        printf("Neuzavrety subor\n");
        return NULL;
    }

    return *fr;
}

//  fukcia prechadza celym suborom a zistuje, kolkokrat sa zopakuje nacitavanie do pola az kym nieje koniec suboru
//  nasledne tento pocet vrati
int zisti_pocet(FILE *fr)
{
    char pomocna[MAX];
    int zisti_pocet = 0;

    rewind(fr);

    while(fgets(pomocna,MAX,fr) != NULL){
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);

            zisti_pocet++;
    }

    rewind(fr);

    return zisti_pocet;
}

//  pomocna funkcia ku qsort-u, funkcia vracia z�porne, nulu alebo kladn� ��slo na z�klade porovnania parametrov(Herout 2.diel, str. 343)
int zisti_porovnaj(const void *prvy, const void *druhy)
{
    return ( *(double*)prvy - *(double*)druhy );
}

//  pred spustenim samotnej funkcie sa v main-e spusti funkcia *otvor(FILE **subor), ktora subor otvori,potom
//  funkcia cita cely subor, nacitava riadky do poli a nasledne ich vypisuje na obrazovku, pricom MAX = 32;
void case_V(FILE *fr)
{
    rewind(fr);

    char transakcia[MAX];
    char kredit[MAX];
    char ucet[MAX];
    char suma[MAX];
    char datum[MAX];
    char medzera[MAX];

    while(fgets(transakcia, MAX, fr) != NULL){
        fgets(kredit, MAX, fr);
        fgets(ucet, MAX, fr);
        fgets(suma, MAX,fr);
        fgets(datum, MAX, fr);
        fgets(medzera, MAX, fr);

        printf("transakcia: %s", transakcia);
        printf("kredit/debet: %s", kredit);
        printf("cislo uctu kam/odkial idu peniaze: %s", ucet);
        printf("suma: %s", suma);
        printf("datum: %s", datum);
        printf("\n");
    }

    rewind(fr);
}

//  funkcia cita cely subor, zistuje najprv najvyssi datum pomocou pola pomocny_datum1, pole_datumov a premennej pomocny_datum2 (datum je
//  prepisany od 4 pozicie, takze su vynechane dni a mesiac);
//  potom sa prechadza znova suborom, porovnavaju sa najvyssi datum s aktualne citanim datumom a ci kredit/debet = 1 a ak to plati,
//  zistuje sa najvyssia suma
void case_R(FILE *fr)
{

    if(fr == NULL){
        return;
    }
        char pomocna[MAX];
        char datum[MAX];
        char suma[MAX];
        char kredit[MAX];
        char pomocny_datum1[MAX];
        int pole_datumov[MAX];
        int i, pomocny_datum2, j = 0, a = 0, MAX_datum = 0, hodnota = 0, counter_datum = 0;
        double pomocna_suma1;
        double pomocna_suma2 = 0.0;
        int zisti_kredit;

        rewind(fr);

        while(!feof(fr)){
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(datum, MAX, fr);
            fgets(pomocna, MAX, fr);

            for(i = 0; i < strlen(pomocny_datum1); i++)
                pomocny_datum1[i] = '\0';
            j = 0;
            for(i = 4; i <= strlen(datum); i++, j++)
                pomocny_datum1[j] = datum[i];

            sscanf(pomocny_datum1, "%d", &pomocny_datum2);

            pole_datumov[a] = pomocny_datum2;
            a++;
            counter_datum++;

        }

        for(i = 0; i < counter_datum; i++){
                if(MAX_datum < pole_datumov[i])
                    MAX_datum = pole_datumov[i];
        }

        rewind(fr);

        while(!feof(fr)){
            fgets(pomocna, MAX, fr);
            fgets(kredit, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(suma, MAX, fr);
            fgets(datum, MAX, fr);
            fgets(pomocna, MAX, fr);

            j = 0;
            for(i = 4; i <= strlen(datum); i++, j++)
                pomocny_datum1[j] = datum[i];

            sscanf(pomocny_datum1, "%d", &hodnota);
            sscanf(kredit, "%d", &zisti_kredit);

            if((hodnota == MAX_datum) && (zisti_kredit != 0)){
                sscanf(suma,"%lf", &pomocna_suma1);
                if(pomocna_suma2 < pomocna_suma1){
                    pomocna_suma2 = pomocna_suma1;
                }
            }

        }

        printf("%.2lf\n", pomocna_suma2);
        rewind(fr);

}

//  vdaka funkcii zisti_pocet poznam pocet udajov v subore, naalokujem podla neho velkost dynamickeho pola a zapisem donho sumy, nasledne
//  pole vratim
double *alokuj_case_N(FILE *fr, int counter)
{
    if(fr == NULL){
        return NULL;
    }

    double *p_pole;
    double pole_suma;
    char pomocna[MAX];
    char suma[MAX];
    int i = 0;

    free((void *)p_pole);
    p_pole = NULL;

    if((p_pole = (double *) malloc(counter * sizeof(double))) == NULL){
        printf("Malo pamate\n");
        return NULL;
    }

    while(fgets(pomocna,MAX,fr) != NULL){
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(suma, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);

            sscanf(suma, "%lf", &pole_suma);

            p_pole[i] = pole_suma;
            i++;
    }

    return p_pole;

}

//  vo funkcii prechadzam suborom a zistujem najvacsiu sumu, podla ktorej budem zarovnavat(porovnaj_sumu)
//  nasledne vyuzivam moznosti prikazu printf aby mi vypisovalo sumy z pola podla najdlhsej sumy(Herout 1.diel, str.210)
void case_S(FILE *fr, int pocitadlo, double *p_pole)
{
    if(p_pole == NULL){
        printf("Pole nieje vytvorene\n");
        return;
    }

    if(fr == NULL){
        return;
    }

    rewind(fr);

    int i;
    char suma[MAX];
    char pomocna[MAX];
    int zisti_sumu;
    int porovnaj_sumu = 0;

    while(fgets(pomocna,MAX,fr) != NULL){
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(suma, MAX, fr);
            fgets(pomocna, MAX, fr);
            fgets(pomocna, MAX, fr);

            zisti_sumu = strlen(suma);

            if(porovnaj_sumu < zisti_sumu)
                porovnaj_sumu = zisti_sumu;
    }
    porovnaj_sumu -= 1;

    for(i = 0; i < pocitadlo; i++)
        printf("%*.2lf\n",porovnaj_sumu , p_pole[i]);

    rewind(fr);

}

//  vo funkcii si do pomocneho pola postupne pocas prechadzania cyklom zapisujem sumy z dynam.pola, zistim si dlzku pom.pola
//  v cykle priradim kazde cislo pocitadlu a v dalsom poli(vypis[]) navysim na danom mieste(hodnote pocitadla) hodnotu o 1, nasledne vypisem
//  histogram z pom.pola vypis[];
void case_H(double *p_pole, int pocitadlo)
{
    if(p_pole == NULL){
        printf("Pole nieje vytvorene\n");
        return;
    }

    char histogram[MAX];
    char vypis[MAX];
    int i,s, counter;
    int j = 0, a = 0,pocitadlo2 = 1;

    for(i = 0; i < MAX; i++){
        histogram[i] = '\0';
        vypis[i] = '\0';
    }

    pocitadlo2 += pocitadlo;

    while(a != pocitadlo2){
    for(i = 0; i < a; i++)
        sprintf(histogram, "%.2lf", p_pole[i]);

    s = strlen(histogram);

    for(i = 0; i < s; i++){
        counter = histogram[i];
        vypis[counter - '0']++;
        counter = 0;
        }
    a++;

    }
    for(i = 0; i < 10; i++){
        printf("%d ", i);
        for(j = 0; j < vypis[i]; j++){
        printf("*");
        }

        putchar('\n');
    }

}

//  funkcia na vzostupne sortovanie dynam.pola(pomocna funkcia je popisana vyssie)
void case_U(double *p_pole, int pocitadlo)
{
    if(p_pole == NULL){
        return;
    }

    qsort(p_pole, pocitadlo, sizeof(double), zisti_porovnaj);
}
